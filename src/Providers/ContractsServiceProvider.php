<?php

namespace Mx\Modules\Providers;

use Illuminate\Support\ServiceProvider;
use Mx\Modules\Contracts\RepositoryInterface;
use Mx\Modules\Laravel\LaravelFileRepository;

class ContractsServiceProvider extends ServiceProvider
{
    /**
     * Register some binding.
     */
    public function register()
    {
        $this->app->bind(RepositoryInterface::class, LaravelFileRepository::class);
    }
}
