<?php

namespace Mx\Modules\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Mx\Modules\Commands\CommandMakeCommand;
use Mx\Modules\Commands\ComponentClassMakeCommand;
use Mx\Modules\Commands\ComponentViewMakeCommand;
use Mx\Modules\Commands\ControllerMakeCommand;
use Mx\Modules\Commands\DisableCommand;
use Mx\Modules\Commands\DumpCommand;
use Mx\Modules\Commands\EnableCommand;
use Mx\Modules\Commands\EventMakeCommand;
use Mx\Modules\Commands\FactoryMakeCommand;
use Mx\Modules\Commands\InstallCommand;
use Mx\Modules\Commands\JobMakeCommand;
use Mx\Modules\Commands\LaravelModulesV6Migrator;
use Mx\Modules\Commands\ListCommand;
use Mx\Modules\Commands\ListenerMakeCommand;
use Mx\Modules\Commands\MailMakeCommand;
use Mx\Modules\Commands\MiddlewareMakeCommand;
use Mx\Modules\Commands\MigrateCommand;
use Mx\Modules\Commands\MigrateRefreshCommand;
use Mx\Modules\Commands\MigrateResetCommand;
use Mx\Modules\Commands\MigrateRollbackCommand;
use Mx\Modules\Commands\MigrateStatusCommand;
use Mx\Modules\Commands\MigrationMakeCommand;
use Mx\Modules\Commands\ModelMakeCommand;
use Mx\Modules\Commands\ModuleDeleteCommand;
use Mx\Modules\Commands\ModuleMakeCommand;
use Mx\Modules\Commands\NotificationMakeCommand;
use Mx\Modules\Commands\PolicyMakeCommand;
use Mx\Modules\Commands\ProviderMakeCommand;
use Mx\Modules\Commands\PublishCommand;
use Mx\Modules\Commands\PublishConfigurationCommand;
use Mx\Modules\Commands\PublishMigrationCommand;
use Mx\Modules\Commands\PublishTranslationCommand;
use Mx\Modules\Commands\RequestMakeCommand;
use Mx\Modules\Commands\ResourceMakeCommand;
use Mx\Modules\Commands\RouteProviderMakeCommand;
use Mx\Modules\Commands\RuleMakeCommand;
use Mx\Modules\Commands\SeedCommand;
use Mx\Modules\Commands\SeedMakeCommand;
use Mx\Modules\Commands\SetupCommand;
use Mx\Modules\Commands\TestMakeCommand;
use Mx\Modules\Commands\UnUseCommand;
use Mx\Modules\Commands\UpdateCommand;
use Mx\Modules\Commands\UseCommand;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Namespace of the console commands
     * @var string
     */
    protected $consoleNamespace = "Nwidart\\Modules\\Commands";

    /**
     * The available commands
     * @var array
     */
    protected $commands = [
        CommandMakeCommand::class,
        ControllerMakeCommand::class,
        DisableCommand::class,
        DumpCommand::class,
        EnableCommand::class,
        EventMakeCommand::class,
        JobMakeCommand::class,
        ListenerMakeCommand::class,
        MailMakeCommand::class,
        MiddlewareMakeCommand::class,
        NotificationMakeCommand::class,
        ProviderMakeCommand::class,
        RouteProviderMakeCommand::class,
        InstallCommand::class,
        ListCommand::class,
        ModuleDeleteCommand::class,
        ModuleMakeCommand::class,
        FactoryMakeCommand::class,
        PolicyMakeCommand::class,
        RequestMakeCommand::class,
        RuleMakeCommand::class,
        MigrateCommand::class,
        MigrateRefreshCommand::class,
        MigrateResetCommand::class,
        MigrateRollbackCommand::class,
        MigrateStatusCommand::class,
        MigrationMakeCommand::class,
        ModelMakeCommand::class,
        PublishCommand::class,
        PublishConfigurationCommand::class,
        PublishMigrationCommand::class,
        PublishTranslationCommand::class,
        SeedCommand::class,
        SeedMakeCommand::class,
        SetupCommand::class,
        UnUseCommand::class,
        UpdateCommand::class,
        UseCommand::class,
        ResourceMakeCommand::class,
        TestMakeCommand::class,
        LaravelModulesV6Migrator::class,
        ComponentClassMakeCommand::class,
        ComponentViewMakeCommand::class,
    ];

    public function register(): void
    {
        $this->commands($this->resolveCommands());
    }

    private function resolveCommands(): array
    {
        $commands = [];

        foreach (config('modules.commands', $this->commands) as $command) {
            $commands[] = Str::contains($command, $this->consoleNamespace) ?
                $command :
                $this->consoleNamespace . "\\" . $command;
        }

        return $commands;
    }

    public function provides(): array
    {
        return $this->commands;
    }
}
